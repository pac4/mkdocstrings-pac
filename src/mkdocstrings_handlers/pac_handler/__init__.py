"""This package implements a handler for the Python language."""

from mkdocstrings_handlers.pac_handler.handler import get_handler

__all__ = ["get_handler"]  # noqa: WPS410
